#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    # TODO: put package requirements here
    'attrs'
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name='pytsctl',
    version='0.1.0',
    description="Python client for Tsctl server",
    long_description=readme + '\n\n' + history,
    author="Michael Kenney",
    author_email='mikek@apl.uw.edu',
    url='https://bitbucket.org/mfkenney/pytsctl',
    packages=[
        'pytsctl',
    ],
    package_dir={'pytsctl':
                 'pytsctl'},
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='pytsctl',
    test_suite='tests',
    tests_require=test_requirements
)

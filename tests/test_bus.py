#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from pytsctl import bus


@pytest.mark.parametrize('cmd,size', [
    ('\x03', 8),
    (b'\x05', 16),
    (b'\x07', 32)
])
def test_peek(cmd, size):
    expect = b"\x01\x00\x00" + cmd + b"\x2a\x00\x00\x00"
    msg = bus.peek_msg(42, size)
    assert msg == expect


@pytest.mark.parametrize('cmd,size,val', [
    (b'\x04', 8, b'\x01'),
    (b'\x06', 16, b'\x01\x02'),
    (b'\x08', 32, b'\x01\x02\x03\x04')
])
def test_poke(cmd, size, val):
    expect = b"\x01\x00\x00" + cmd + b"\x2a\x00\x00\x00" + val
    msg = bus.poke_msg(42, size, 0x04030201)
    assert msg == expect

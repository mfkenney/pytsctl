#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pytsctl import dio


def test_get():
    expect = b"\x05\x00\x00\x08\x2a\x00\x00\x00"
    msg = dio.getasync_msg(42)
    assert msg == expect


def test_set():
    expect = b"\x05\x00\x00\x07\x2a\x00\x00\x00\x01\x00\x00\x00"
    msg = dio.setasync_msg(42, dio.State.high)
    assert msg == expect

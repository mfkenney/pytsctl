#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pytsctl import system


def test_lookup():
    expect = b"\x00\x00\x00\x0c\x03\x00\x00\x00Foo"
    msg = system.map_lookup_msg('Foo')
    assert msg == expect

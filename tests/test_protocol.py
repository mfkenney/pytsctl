#!/usr/bin/env python
# -*- coding: utf-8 -*-
from io import BytesIO
import pytsctl.protocol as tp


def test_unpack():
    src = BytesIO(b"\x00\x00\x11\x50\x04\x00\x00\x001.42\x80")
    r = tp.get_reply(src)
    assert len(r) == 1
    assert r[0] == b"1.42"


def test_pack():
    wire = b"\x00\x00\x00\x0c\x03\x00\x00\x00foo"
    buf = tp.pack_req(tp.TsReq(0, 0, 0x0c), ('I', 3), b'foo')
    assert buf == wire

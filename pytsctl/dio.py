# -*- coding: utf-8 -*-
"""
.. module:: pytsctl.dio
     :platform: any
     :synopsis: implement the Dio class messages
"""
from __future__ import absolute_import
from .protocol import TsReq, TsClass, pack_req


class Command:
    lock = 0
    unlock = 1
    dio_prempt = 2
    dio_refresh = 3
    dio_commit = 4
    dio_set = 5
    dio_get = 6
    dio_set_async = 7
    dio_get_async = 8
    dio_wait = 9
    dio_count = 10
    dio_capabilities = 11
    dio_get_multi = 12


class State:
    input_low = -3
    input_high = -2
    input = -1
    low = 0
    high = 1


def getasync_msg(n):
    """
    Construct a Dio Get Async message.

    :param n: Dio line number.
    :type n: int
    """
    return pack_req(TsReq(TsClass.dio, 0, Command.dio_get_async), ('I', n))


def setasync_msg(n, state):
    """
    Construct a Dio Set Async message.

    :param n: Dio line number.
    :type n: int
    :param state: new Dio state
    :type state: int (see :cls:`State`)
    """
    return pack_req(TsReq(TsClass.dio, 0, Command.dio_set_async),
                    ('I', n), ('i', state))

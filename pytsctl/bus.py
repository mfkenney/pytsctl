# -*- coding: utf-8 -*-
"""
.. module:: pytsctl.bus
     :platform: any
     :synopsis: implement the Bus class messages
"""
from __future__ import absolute_import
from .protocol import TsReq, TsClass, pack_req


class Command:
    lock = 0
    unlock = 1
    prempt = 2
    peek8 = 3
    poke8 = 4
    peek16 = 5
    poke16 = 6
    peek32 = 7
    poke32 = 8
    bit_get8 = 9
    bit_assign8 = 10
    bit_set8 = 11
    bit_clear8 = 12
    bit_get16 = 13
    bit_assign16 = 14
    bit_set16 = 15
    bit_clear16 = 16
    bit_get32 = 17
    bit_assign32 = 18
    bit_set32 = 19
    bit_clear32 = 20
    peek_stream = 21
    poke_stream = 22
    refresh = 23
    commit = 24
    bit_toggle8 = 25
    bit_toggle16 = 26
    bit_toggle32 = 27
    assign8x = 28
    assign16x = 29
    assign32x = 30
    bits_get8 = 31
    bits_get16 = 32
    bits_get32 = 33


def bus_lock_msg(lock_type):
    """
    Construct a Bus Lock message.

    :param lock_type: lock type code (see :cls:`protocol.TsLock`)
    :returns: message byte string
    """
    return pack_req(TsReq(TsClass.bus, 0, Command.lock),
                    ('I', 0), ('I', lock_type))


def bus_unlock_msg(lock_type):
    """
    Construct a Bus Unlock message.

    :param lock_type: lock type code (see :cls:`protocol.TsLock`)
    :returns: message byte string
    """
    return pack_req(TsReq(TsClass.bus, 0, Command.unlock),
                    ('I', 0), ('I', lock_type))


def peek_msg(address, size):
    """
    Construct a Bus Peek message.

    :param address: bus address
    :param size: number of bits to read; 8, 16, or 32
    :returns: message byte string
    """
    if size == 8:
        cmd = Command.peek8
    elif size == 16:
        cmd = Command.peek16
    elif size == 32:
        cmd = Command.peek32
    else:
        raise ValueError('Invalid size: {0:d}'.format(size))
    return pack_req(TsReq(TsClass.bus, 0, cmd), ('I', address))


def poke_msg(address, size, value):
    """
    Construct a Bus Poke message.

    :param address: bus address
    :param size: number of bits to write; 8, 16, or 32
    :param value: integer value to write
    :returns: message byte string
    """
    if size == 8:
        cmd, dtype = Command.poke8, 'B'
    elif size == 16:
        cmd, dtype = Command.poke16, 'H'
    elif size == 32:
        cmd, dtype = Command.poke32, 'I'
    else:
        raise ValueError('Invalid size: {0:d}'.format(size))

    mask = long((1 << size) - 1)
    return pack_req(TsReq(TsClass.bus, 0, cmd),
                    ('I', address), (dtype, value & mask))

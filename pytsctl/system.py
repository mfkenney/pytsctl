# -*- coding: utf-8 -*-
"""
.. module:: pytsctl.system
     :platform: any
     :synopsis: implement the System class messages
"""
from __future__ import absolute_import
from .protocol import TsReq, TsClass, pack_req


class Command:
    class_count = 0
    instance_count = 1
    api_count = 2
    lock_count = 3
    lock_holder_info = 4
    conn_wait_info = 5
    can_bus_get = 6
    build_time = 7
    model_id = 8
    base_board_id = 9
    map_length = 10
    map_get = 11
    map_lookup = 12
    map_lookup_partial = 13
    map_add = 14
    map_delete = 15
    note = 16
    version = 17
    uptime_server = 18
    uptime_host = 19
    fpga_revision = 20
    echo_number = 21


def map_lookup_msg(name):
    """
    Construct a System Map Lookup message.

    The server maintains a map which associates string names with integer
    values. This feature is mainly used to store the names of DIO lines.

    :param name: item name
    :type name: string
    :returns: message byte string
    """
    n = len(name)
    return pack_req(TsReq(TsClass.system, 0, Command.map_lookup),
                    ('I', n),
                    name)

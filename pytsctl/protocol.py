# -*- coding: utf-8 -*-
"""
.. module:: pytsctl.protocol
     :platform: any
     :synopsis: implement the client side of the tsctl TCP protocol
"""
import attr
import struct

TSCTL_PORT = 5001


class UnsupportedType(ValueError):
    pass


class TsClass:
    system = 0
    bus = 1
    time = 2
    pin = 3
    dioraw = 4
    dio = 5
    twi = 6
    can = 7
    spi = 8
    aio = 9
    edio = 10


class TsTag:
    strlen = (0x50,)
    intval = (0x13, 0xc4, 0xc0, 0x03)
    end = (0x80,)
    int_array = (0x53,)
    byte = (0x00,)
    word = (0x01,)


class TsLock:
    non_blocking = 1
    shared = 2
    no_unlock = 4


def packable(cls):
    """
    Decorator to add packed string support (using struct) to an
    attrs based class. Adds a method named *pack* which will
    return a pack binary string representation of the object
    using the *_fmt* attribute as the packing format.
    """
    def packer(self):
        fields = [getattr(self, a.name) for a in cls.__attrs_attrs__ if a.repr]
        return struct.pack(self._fmt, *fields)
    if not hasattr(cls, '_fmt'):
        raise ValueError('Class {name} missing _fmt attribute'
                         .format(name=cls.__name__))
    cls.pack = packer
    return cls


def unpackable(cls):
    """
    Decorator to add string unpacking support (using struct) to an
    attrs based class. Adds two class-methods, *unpack* and *size*.
    The *size* method returns the number of bytes required for the
    packed representation. The *unpack* method is a "factory function"
    which creates a new class instance from a binary string.
    """
    def unpacker(klass, s):
        vals = struct.unpack(klass._fmt.default, s)
        return klass(*vals)

    def size(klass):
        return struct.calcsize(klass._fmt.default)

    if not hasattr(cls, '_fmt'):
        raise ValueError('Class {name} missing _fmt attribute'
                         .format(name=cls.__name__))
    cls.unpack = classmethod(unpacker)
    cls.size = classmethod(size)
    return cls


@packable
@attr.s
class TsReq(object):
    """
    Header for a tsctl command.
    """
    _fmt = attr.ib(init=False, repr=False, default='<HBB')
    tsclass = attr.ib()
    inst = attr.ib()
    command = attr.ib()


@unpackable
@attr.s
class TsReply(object):
    """
    Header for a tsctl reply.
    """
    _fmt = attr.ib(init=False, repr=False, default='<HBB')
    tsclass = attr.ib()
    command = attr.ib()
    tag = attr.ib()


def get_reply(src):
    """
    Read a reply from the server.

    :param src: object with a *read* method
    :returns: a list of values
    """
    b = src.read(TsReply.size())
    hdr = TsReply.unpack(b)
    tag = hdr.tag
    payload = []
    while tag not in TsTag.end:
        if tag in TsTag.byte:
            payload.append(ord(src.read(1)))
        elif tag in TsTag.word:
            payload.append(struct.unpack('<H', src.read(2))[0])
        elif tag in TsTag.intval:
            payload.append(struct.unpack('<i', src.read(4))[0])
        elif tag in TsTag.strlen:
            n = struct.unpack('<I', src.read(4))[0]
            payload.append(src.read(n))
        elif tag in TsTag.int_array:
            n = struct.unpack('<I', src.read(4))[0]
            fmt = '<{0:d}i'.format(n)
            payload.append(struct.unpack(fmt, src.read(4*n)))
        else:
            raise UnsupportedType('Tag = {0:02x}'.format(b))
        tag = ord(src.read(1))
    return payload


def pack_str(s):
    n = len(s)
    fmt = '<I{0:d}s'.format(n)
    return struct.pack(fmt, n, s)


def pack_req(hdr, *args):
    """
    Pack a tsctl request into a binary string.

    :param hdr: header object
    :type hdr: TsReq
    :param args: each argument is either a string or a tuple
                 of (format, value) where format is a struct
                 compatible format character.
    """
    body = []
    for a in args:
        try:
            dtype, val = a
            body.append(struct.pack('<' + dtype, val))
        except ValueError:
            body.append(a.encode('us-ascii'))
    return hdr.pack() + b''.join(body)

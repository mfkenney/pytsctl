# -*- coding: utf-8 -*-
"""
.. module:: pytsctl.client
     :platform: any
     :synopsis: TCP client implementation
"""
from __future__ import absolute_import
import socket
from array import array
from .protocol import TSCTL_PORT, get_reply


class TsClient(object):
    def __init__(self, host='localhost', port=TSCTL_PORT, timeout=None):
        """
        Initialize TsClient instance and open a connection to the server.

        :param host: server hostname or IP address
        :param port: server TCP port
        :param timeout: socket timeout in seconds
        :type timeout: float
        """
        self.host = host
        self.port = port
        self.timeout = timeout
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(self.timeout)
        self.sock.connect((self.host, self.port))

    def reconnect(self):
        """
        Close and reopen the connection to the server.
        """
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(self.timeout)
        self.sock.connect((self.host, self.port))

    def read(self, nbytes):
        """
        Read ``nbytes`` bytes from the server.

        :rtype: string
        :raises socket.timeout: if a read timeout occurs
        """
        buf = array('c')
        while nbytes > 0:
            data = self.sock.recv(nbytes)
            if not data:
                break
            buf.extend(data)
            nbytes -= len(data)
        return buf.tostring()

    def sendreq(self, msg):
        """
        Send a request to the server and return the response.

        :param msg: request message
        :type msg: byte string
        :returns: server response
        :rtype: list
        :raises socket.timeout: if a read timeout occurs
        :raises UnsupportedType: on an unknown server response
        """
        self.sock.send(msg)
        return get_reply(self)

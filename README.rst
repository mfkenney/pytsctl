===============================
PyTsctl
===============================

This package provides a Python client for the `Technologic Systems`_
control server known as tsctl_. This program is used to provide a uniform
user-space interface to the various subsystems of their embedded ARM
boards.

.. _`Technologic Systems`: http://www.embeddedarm.com/index.php
.. _tsctl: http://wiki.embeddedarm.com/wiki/Tsctl

Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
